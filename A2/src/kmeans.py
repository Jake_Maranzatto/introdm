#A template for the implementation of K-Means.
import math #sqrt
import random
from graphics import *

win = GraphWin('', 510,510)
win.setCoords(0,0,1,1)

# Accepts two data points a and b.
# Returns the euclidian distance 
# between a and b.
def euclidianDistance(a,b):
    return math.sqrt(sum([(i - j)**2 for i,j in zip(a,b)]))

# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
#  to center c.
def assignClusters(D, centers):
    dic = {c:[] for c in centers}
    for d in D:
        center = None
        for c in centers:
            if center == None:
                center = c
            else:
                if euclidianDistance(d,c) < euclidianDistance(d,center):
                    center = c
        dic[center].append(d)

# Accepts a list of data points.
# Returns the mean of the points.
def findClusterMean(cluster):
    total = None
    for datapoint in cluster:
        if total == None:
            total = list(datapoint)
        else:
            for j in range(len(datapoint)):
                total[j] += datapoint[j]
     #return scaled sum           
    return tuple([i / len(cluster) for i in total])
            
# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Means clustering
#  of D.
def KMeans(D, k):
    means = D[0:k]
    while True:
        print('looping')
        oldMeans = means[:]
        for k in oldMeans:
            Circle(Point(*k), .005).draw(win).setFill("blue")
        clusters = {}
        #finding nearest center
        for d in D:
            nearest = means[0]
            for m in means:
                if euclidianDistance(d,m) < euclidianDistance(nearest, d):
                    nearest = m
            #this may freak out
            try:
                clusters[nearest].append(d)
            except:
                clusters[nearest] = [d]
        #get new means
        for i in range(len(means)):
            means[i] = findClusterMean(clusters[means[i]])
        #checking for exit condition
        if means == oldMeans:
            return clusters

D = []
for i in range(10000):
    p1 = random.random()
    p2 = random.random()
    #Circle(Point(p1,p2), 0.0025).draw(win)
    D.append((p1,p2))

    
for k in KMeans(D, 10):
    Circle(Point(*k), .01).draw(win).setFill("red")



    


