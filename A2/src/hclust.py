#A template for the implementation of hclust
import math #sqrt
import random
import copy
from graphics import *
import time
import heapq
#setting up a simple visualization
scale = 500
win = GraphWin('', 10 + scale,10 + scale)

#defining a tree object class
#simple implementation modified from https://stackoverflow.com/questions/2358045/how-can-i-implement-a-tree-in-python-are-there-any-built-in-data-structures-in
#everything will be implemented using this class
#this will result in O(N) slowdown, but makes implementation much cleaner
class Tree(object):
    def __init__(self, left = None, right = None, data = None):
        self.left = None
        self.right = None
        if data != None:
            self.data = data

    #extends the tree so that the left side is this tree
    #and the right side is the passed node
    def drawconnection(self, node):
        if len(self.data) == 2 and len(self.data) == 2:
            Line(Point(*self.data), Point(*node.data)).draw(win).setWidth(1)
        
    def draw(self, radius, color = "white"):
        if len(self.data) == 2:
            Circle(Point(*self.data), radius).draw(win).setFill(color)

    def merge(self, node):
        if self == node:
            print('error')
        
        self.drawconnection(node)
        self.left = copy.copy(self)
        self.right = node
        self.data = [(i + j)/2 for i,j in zip(self.data,node.data)]
    def getLeafData(self):
        if self.left == None and self.right == None:
            return self.data
        else:
            return(self.left.getLeafData(),self.right.getLeafData())
            
        
# Accepts two nodes a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.

def Distance(a,b):
    #assuming data has the form p = [x0, x1, x2... xn], a vector in the Tree class
    #returning euclidian distance
        return math.sqrt(sum([(i - j)**2 for i,j in zip(a.data,b.data)]))

##a = Tree(data = [0,0])
##b = Tree(data = [1,1])
##merge(a,b)
##print(a.data)

# Accepts a list of nodes.
# Returns the pair of points that are closest
#runs in O(n^2) time
def findClosestPair(D):
    min_dist = None
    pair = []
    for i in range(len(D)):
        for j in range(i + 1, len(D)):
            dist = Distance(D[i], D[j])
            #if not initialized
            if min_dist == None:
                min_dist = dist
                pair = [i,j]
                
            #if distance is less
            elif  dist < min_dist:
                min_dist = dist
                pair = [i,j]
    return pair

def mergeClosestPair(D, pair):
    D[pair[0]].merge(D[pair[1]])
    del D[pair[1]]

# Accepts a list of nodes.
# Produces a tree structure corresponding to a 
# Agglomerative Hierarchal clustering of D.

#here make sure D is a list of trees (nodes)


#recursive definition for our function (O(N^3))
def HClustRecursive(D):
    #if we only have one element our tree is done
    if len(D) <= 1:
        return D[0]
    else:
        #finding the closest pair and merging them
        #O(n^2)
        p = findClosestPair(D)
        mergeClosestPair(D, p)
        return(HClust(D))

#loop definition for our function (O(N^3))
def HClust(D):
    while len(D) > 1:
        p = findClosestPair(D)
        mergeClosestPair(D, p)
    return D
    
#heap definition for our function (O(N^2 * Log(N)))

def heapFromData(D):
    heap = []
    #initializing our heap O(N^2)
    for i in range(len(D)):
            for j in range(i + 1, len(D)):
                heapq.heappush(heap, (Distance(D[i], D[j]), D[i], D[j]))
    return heap

def HClustHeap(D, hybridparam):
    heap = heapFromData(D)
    init_len = len(D)

    while len(D) > hybridparam:
        #if we have reduced the size of our list alot
        #we refresh our heap
        #can change this parameter around
        if len(D) == init_len/3:
            init_len = init_len / 3
            heap = heapFromData(D)

            
        p = heapq.heappop(heap) 
        if p[1] in D and p[2] in D and Distance(p[1], p[2]) == p[0]:
            p[1].merge(p[2])
            D.remove(p[2])
        
            for data in D:
                #checking if our index is the newly merged cluster
                if data == p[1]:
                    continue
                heapq.heappush(heap, (Distance(data, p[1]), p[1], data))
        else:
            continue
    return D

def hybridHClust(D, hybridparam):
    HClustHeap(D, hybridparam)
    HClust(D)
    return D
                       


                       
#getting timings of both functions
for j in range(5, 12):
    win.delete("all")
    twotoj = 2**j
    print('List length is ' + str(twotoj))
    D = []
    #generate random clusters
    for i in range(twotoj):
        if i%2 == 0:
            datapoint = Tree(data = [scale/3*random.random(),scale - scale/2*random.random()])
        else:
            datapoint = Tree(data = [scale - scale/3*random.random(),scale/2*random.random()])

        datapoint.draw(radius = 2)
        D.append(datapoint)

    M = copy.deepcopy(D)
    #testing the timings of the different implementations
    start1 = time.time()
    HClustHeap(M, 1)
    end1 = time.time()
    time.sleep(2)
    win.delete("all")
    [datapoint.draw(radius = 2) for datapoint in D]
    start2 = time.time()
    hybridHClust(D, 100)
    end2 = time.time()
    time.sleep(2)
    print('heap took: ' + str(end1 - start1) + ' hyrbid took: ' + str(end2 - start2))












				



