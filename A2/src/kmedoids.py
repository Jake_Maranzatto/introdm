#A template for the implementation of K-Medoids.
import math #sqrt
import random
from graphics import *

win = GraphWin('', 510,510)
win.setCoords(0,0,1,1)

# Accepts two data points a and b.
# Returns the distance between a and b.
# Note that this might be specific to your data.
def Distance(a,b):
    #returning manhattan distance
    return (sum([max(i,j) - min(i,j) for i,j in zip(a,b)]))

# Accepts a list of data points D, and a list of centers
# Returns a dictionary of lists called "clusters", such that
# clusters[c] is a list of every data point in D that is closest
#  to center c.
# Note: This can be identical to the K-Means function of the same name.
def assignClusters(D, centers):
    dic = {c:[] for c in centers}
    for d in D:
        center = None
        for c in centers:
            if center == None:
                center = c
            else:
                if Distance(d,c) < Distance(d,center):
                    center = c
        dic[center].append(d)
    return dic
            
# Accepts a list of data points.
# Returns the medoid of the points.
def findClusterMedoid(cluster):
    #assigning an arbitrary medoid
    medoid = cluster[0]
    
    #checking if any other point is a better fit
    for c in cluster:
        medoid_dist = 0
        cand_dist = 0
        for cand in cluster:
            medoid_dist += Distance(medoid, cand)
            cand_dist += Distance(c, cand)
        if cand_dist < medoid_dist:
            medoid = c

    return medoid


# Accepts a list of data points, and a number of clusters.
# Produces a set of lists representing a K-Medoids clustering
#  of D.
def KMedoids(D, k):
    centers = D[0:k]
    while True:
        print('looping')
        oldCenters = centers[:]
        clusters = assignClusters(D, centers)
        for i,c in enumerate(centers):
            Circle(Point(*c), .005).draw(win).setFill("blue")
        #finding nearest centers
            centers[i] = findClusterMedoid(clusters[c])
        #checking for exit condition
        if centers == oldCenters:
            return clusters		


D = []
for i in range(1000):
    p1 = random.random()
    p2 = random.random()
    Circle(Point(p1,p2), 0.0025).draw(win)
    D.append((p1,p2))

    
for k in KMedoids(D, 10):
    Circle(Point(*k), .01).draw(win).setFill("red")


