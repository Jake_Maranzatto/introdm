  CHAPTER  
  Loomings
   Call Ishmael
  Some years ago never mind long precisely having little money purse  nothing particular interest me on shore  I thought I would sail little see watery part of world
  It way I driving spleen and regulating circulation
  Whenever I find growing grim about the mouth  whenever damp  drizzly November soul  whenever I find involuntarily pausing coffin warehouses  and bringing rear every funeral I meet  especially whenever my hypos get upper hand me  requires strong moral principle prevent deliberately stepping street  and methodically knocking people s hats off then  I account high time to get sea soon I
  This substitute pistol ball
  With philosophical flourish Cato throws upon sword  I quietly take ship
  There nothing surprising
  If they but knew it  almost men degree  time other  cherish nearly feelings towards ocean
   There insular city Manhattoes  belted round by wharves Indian isles coral reefs commerce surrounds her surf
  Right left  streets take waterward
  Its extreme downtown battery  noble mole washed waves  and cooled breezes  hours previous sight of land
  Look crowds water gazers
   Circumambulate city dreamy Sabbath afternoon
  Go Corlears Hook Coenties Slip  thence  Whitehall  northward
  What do see  Posted like silent sentinels around town  stand thousands upon thousands mortal men fixed ocean reveries
  Some leaning spiles  seated upon pier heads  some looking bulwarks ships China  high aloft the rigging  striving get still better seaward peep
  But these are landsmen  week days pent lath plaster tied to counters  nailed benches  clinched desks
  How this  Are the green fields gone  What here   But look  come crowds  pacing straight water  and seemingly bound dive
  Strange  Nothing content the extremest limit land  loitering shady lee yonder warehouses suffice
  No
  They must get nigh water as possibly without falling
  And stand miles of them leagues
  Inlanders all  come lanes alleys  streets and avenues north  east  south  west
  Yet unite
  Tell me  magnetic virtue needles compasses all those ships attract thither   Once
  Say country  high land lakes
  Take almost path please  ten one carries a dale  leaves pool stream
  There magic in it
  Let absent minded men plunged deepest reveries stand man legs  set feet a going  will infallibly lead water  water region
  Should ever athirst great American desert  try this experiment  caravan happen supplied metaphysical professor
  Yes  every one knows  meditation water wedded for ever
   But artist
  He desires paint dreamiest  shadiest  quietest  enchanting bit romantic landscape valley of Saco
  What chief element employs  There stand his trees  hollow trunk  hermit crucifix were within  sleeps meadow  sleep cattle  up from yonder cottage goes sleepy smoke
  Deep distant woodlands winds mazy way  reaching overlapping spurs mountains bathed in their hill side blue
  But though picture lies thus tranced  and though pine tree shakes sighs like leaves upon this shepherd s head  yet vain  unless shepherd s eye were fixed upon magic stream
  Go visit Prairies June  when scores scores miles wade knee deep among Tiger lilies what one charm wanting  Water there drop of water there  Were Niagara cataract sand  would travel your thousand miles see it  Why poor poet Tennessee  upon suddenly receiving two handfuls silver  deliberate whether buy him coat  sadly needed  invest money pedestrian trip Rockaway Beach  Why almost every robust healthy boy a robust healthy soul him  time crazy go sea  Why upon first voyage passenger  feel a mystical vibration  first told ship out of sight land  Why old Persians hold sea holy  Why did the Greeks give separate deity  brother Jove  Surely all without meaning
  And still deeper meaning that story Narcissus  could grasp tormenting  mild image saw fountain  plunged drowned
  But that same image  see rivers oceans
  It image of ungraspable phantom life  key
   Now  I say I habit going sea whenever I begin to grow hazy eyes  begin conscious my lungs  I mean inferred I ever go sea a passenger
  For go passenger must needs purse  a purse rag unless something
  Besides  passengers get sea sick grow quarrelsome don t sleep nights do enjoy themselves much  general thing  no  I never go passenger  nor  though I something salt  I ever go sea a Commodore  Captain  Cook
  I abandon glory distinction of offices like
  For part  I abominate all honorable respectable toils  trials  tribulations every kind whatsoever
  It quite much I take care myself  without taking care ships  barques  brigs  schooners 
  And going cook  though I confess considerable glory in that  cook sort officer ship board yet  somehow  I never fancied broiling fowls  though broiled  judiciously buttered  judgmatically salted peppered  one who will speak respectfully  say reverentially  broiled fowl I
  It idolatrous dotings old Egyptians upon broiled ibis roasted river horse  see the mummies creatures huge bake houses pyramids
   No  I go sea  I go simple sailor  right mast  plumb forecastle  aloft royal mast head
  True  rather order some  make jump spar to spar  like grasshopper May meadow
  And first  sort of thing unpleasant enough
  It touches one s sense honor  particularly come old established family land  the Van Rensselaers  Randolphs  Hardicanutes
  And all  if just previous putting hand tar pot  been lording country schoolmaster  making tallest boys stand in awe
  The transition keen one  I assure you  a schoolmaster sailor  requires strong decoction Seneca and the Stoics enable grin bear
  But even wears off in time
   What it  old hunks sea captain orders get broom and sweep decks  What indignity amount to  weighed  I mean  scales New Testament  Do think archangel Gabriel thinks anything less me  I promptly and respectfully obey old hunks particular instance  Who ain t a slave  Tell
  Well  then  however old sea captains may order about however may thump punch about  I the satisfaction knowing right  everybody else is one way served much way either physical or metaphysical point view  is  universal thump is passed round  hands rub other s shoulder blades  and content
   Again  I always go sea sailor  make point of paying trouble  whereas never pay passengers single penny I ever heard
  On contrary  passengers must pay
  And difference world paying and being paid
  The act paying perhaps uncomfortable infliction two orchard thieves entailed upon us
  But  being paid   what compare it  The urbane activity man receives money really marvellous  considering earnestly believe money root earthly ills  no account monied man enter heaven
  Ah  cheerfully consign ourselves perdition   Finally  I always go sea sailor  wholesome exercise pure air fore castle deck
  For world  head winds far prevalent winds astern  that is  if you never violate Pythagorean maxim   part the Commodore quarter deck gets atmosphere second hand from the sailors forecastle
  He thinks breathes first  not so
  In much way commonalty lead leaders many other things  time leaders little suspect
  But wherefore repeatedly smelt sea a merchant sailor  I take head go whaling voyage  invisible police officer Fates  the constant surveillance me  secretly dogs me  influences in some unaccountable way he better answer one else
  And  doubtless  going whaling voyage  formed part grand programme Providence drawn long time ago
  It came in as sort brief interlude solo extensive performances
  I take part bill must run something like this     Grand Contested Election Presidency United States
    WHALING VOYAGE BY ONE ISHMAEL
   BLOODY BATTLE IN AFFGHANISTAN
    Though I cannot tell exactly stage managers  the Fates  put shabby part whaling voyage  when others set magnificent parts high tragedies  short and easy parts genteel comedies  jolly parts farces though I cannot tell exactly  yet  I recall the circumstances  I think I see little springs motives which cunningly presented various disguises  induced me set performing part I did  besides cajoling the delusion choice resulting unbiased freewill and discriminating judgment
   Chief among motives overwhelming idea great whale himself
  Such portentous mysterious monster roused my curiosity
  Then wild distant seas rolled island bulk  undeliverable  nameless perils whale  these  all the attending marvels thousand Patagonian sights sounds  helped sway wish
  With men  perhaps  things would inducements  me  I tormented an everlasting itch things remote
  I love sail forbidden seas  and land barbarous coasts
  Not ignoring good  I quick to perceive horror  could still social it would let me since well friendly terms inmates of the place one lodges
   By reason things  then  whaling voyage welcome  the great flood gates wonder world swung open  wild conceits swayed purpose  two two floated into my inmost soul  endless processions whale  and  mid them all  one grand hooded phantom  like snow hill air
    CHAPTER  
  The Carpet Bag
   I stuffed shirt two old carpet bag  tucked my arm  started Cape Horn Pacific
  Quitting good city of old Manhatto  I duly arrived New Bedford
  It Saturday night in December
  Much I disappointed upon learning little packet Nantucket already sailed  way reaching that place would offer  till following Monday
   As young candidates pains penalties whaling stop at this New Bedford  thence embark voyage  may well be related I  one  idea
  For mind was made sail Nantucket craft  a fine  boisterous something everything connected famous old island  amazingly pleased
  Besides though New Bedford has of late gradually monopolising business whaling  though in matter poor old Nantucket much behind her  yet Nantucket was great original the Tyre Carthage  the place the first dead American whale stranded
  Where else Nantucket did aboriginal whalemen  Red Men  first sally canoes to give chase Leviathan  And Nantucket  too  did that first adventurous little sloop put forth  partly laden with imported cobblestones so goes story to throw whales  in order discover nigh enough risk harpoon the bowsprit   Now night  day  still another night following me in New Bedford  ere I could embark destined port  became a matter concernment I eat sleep meanwhile
  It a very dubious looking  nay  dark dismal night  bitingly cold and cheerless
  I knew one place
  With anxious grapnels I had sounded pocket  brought pieces silver  So  wherever go  Ishmael  said I myself  I stood middle of a dreary street shouldering bag  comparing gloom towards the north darkness towards south wherever wisdom you may conclude lodge night  dear Ishmael  sure to inquire price  don t particular
   With halting steps I paced streets  passed sign  The Crossed Harpoons  but looked expensive jolly
  Further on  bright red windows  Sword Fish Inn   came such fervent rays  seemed melted packed snow and ice house  everywhere else congealed frost lay ten inches thick hard  asphaltic pavement  rather weary me  when I struck foot flinty projections  from hard  remorseless service soles boots most miserable plight
  Too expensive jolly  thought I  pausing one moment watch broad glare street  hear sounds of the tinkling glasses within
  But go on  Ishmael  said I last  don t you hear  get away door  patched boots are stopping way
  So I went
  I instinct followed streets that took waterward  there  doubtless  cheapest  not the cheeriest inns
