#Template for Assignment 1.
#Author: Jake Maranzatto
#Date:
#Note: This implementation is not very efficient. 
# Hint: @lru_cache(maxsize=None) is likely to be a 
#   favourable decoration for some functions.
import openpyxl
from functools import lru_cache

import re
import io
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize

wb = openpyxl.load_workbook(filename = 'C:\\Users\\Jake From State Farm\\Documents\\GitHub\\introdm\\A1\\data\\cleandata500000.xlsx', read_only = True)
ws = wb.active


MOBY_DICK_DATABASE = []
#filtering text's stop words and stemming words
##ps = PorterStemmer()
stop_words = set(stopwords.words('english'))
##file1 = open('C:\\Users\\Jake From State Farm\\Desktop\\moby dick.txt', encoding="utf8")
##line = file1.read()
##sentences =line.split('.')
##file1.close()

regex = re.compile('[^a-zA-Z]')
appendFile = open('testFull.txt')
data = appendFile.read()
sentences = data.split('\n')
counter = 0
for s in sentences:
##    if counter > 100:
##        print('test done')
##        break
    words = s.split(' ') 
    MOBY_DICK_DATABASE.append(frozenset())
    for r in words:
        if r == '':
            continue
        try:
            r = regex.sub(' ', r)
            if not r in stop_words:
                MOBY_DICK_DATABASE[-1] = MOBY_DICK_DATABASE[-1].union(frozenset([r.lower()]))
                #appendFile.write(" "+r)
        except:
            print('bad character')
    #appendFile.write('\n')
    counter +=1
print('done')
appendFile.close()

#               B                                   C                               E               F                                   G                     
#	case	call description	police zone	police grid	city council

#DATA PROCESSING
def databaseFromExcel():
    database = tuple(set() for i in range(164))
    failed = 0
    for y_row in ws.iter_rows(min_row = 2, max_row = 502, min_col = 3, max_col = 5):
    #for i in range(2, 5002):
        pgrid = y_row[2].value
        crime = y_row[0].value
        try:
            database[pgrid].add(frozenset([crime]))
        except: failed +=1
    return database
#returns the list of unique items from the database
#database: A list of sets of items

def itemsFromDatabase(database):
    items = frozenset()
    for frozset in database:
        items = items.union(frozset)
    return items

#Computes the support of the given itemset in the given database.
#itemset: A set of items
#return: The number of sets in the database which itemset is a subset of.
def support(itemset, database):
    sup = 0
    for frozset in database:
        if itemset.issubset(frozset):
            sup += 1
    return sup

#Computes the confidence of a given rule.
#The rule takes the form precedent --> antecedent
#precedent: A set of items
#antecedent: A set of items that is a superset of precedent
#database: a list of sets of items.
#return: The confidence in precedent --> antecedent.
def confidence(precedent, antecedent, database):    
    return (support(antecedent, database) / support(precedent, database))

#Finds all itemsets in database that have at least minSupport.
#database: A list of sets of items.
#minSupport: an integer > 1
#return: A list of sets of items, such that 
#   s in return --> support(s,database) >= minSupport.

#@lru_cache(maxsize=None)
def findFrequentItemsets(database, minSupport):
    freqSets = []
    items = itemsFromDatabase(database)
    cands = [frozenset([i]) for i in items]
    need_break = 0
    counter = 0
    while len(cands) > 0:
        print('still going')
        if False:
            break
        counter += 1
        H = []
        for c in cands:
            if(c not in H and support(c, database) >= minSupport):
                H.append(c)
                
        cands = []
        
        #checking for memory error and array overload
        #while not in apriori, this just makes sure the code
        #will actually execute, we will be missing some larger sets with this implementation
        if len(freqSets) + len(H) < 500000000:
            freqSets = freqSets + H
        else:
            max_length = 500000000 - len(freqSets)
            freqSets = freqSets + H[0:max_length]
            need_break = 1
            
        #creating a copy of H so we don't mess up FreqSets
        tempH = H.copy()

        #generating unions of our sets for cands
        #again, having to check for memory overflow issues
        for h in tempH:
            if len(cands) > 2000000:
                break
            tempH.remove(h)
            for i in tempH:
                    if len(cands) > 2000000:
                        break
                    cands.append(h.union(i))

        #checking to see if our break condition has been met
        if need_break ==1:
            break
    
    return freqSets
                
#Given a set of frequently occuring Itemsets, returns
# a list of pairs of the form (precedent, antecedent)
# such that for every returned pair, the rule 
# precedent --> antecedent has confidence >= minConfidence
# in the database.
#frequentItemsets: a set or list of sets of items.
#database: A list of sets of items.
#minConfidence: A real value between 0.0 and 1.0. 
#return: A set or list of pairs of sets of items.
def findRules(frequentItemsets, database, minConfidence):
    print('finding rules now...')
    rules = []
    tempFreq = frequentItemsets.copy()
    for s in frequentItemsets:
        print('Has not crashed... in finding rules...')
        tempFreq.remove(s)
        for t in tempFreq:
            #print(s,t, t.union(s), confidence(s, t.union(s), database))
            if confidence(s, t.union(s), database) >= minConfidence:
                if len(rules) < 1000:
                        rules.append((s,t))
    return rules

#Produces a visualization of frequent itemsets.
def visualizeItemsets(frequentItemsets):
    return 0

#Produces a visualization of rules.
def visualizeRules(rules):
    return 0


#Here's a simple test case:
#database = (frozenset([1,2,3]), frozenset([2,3]), frozenset([4,5]), frozenset([1,2]),frozenset([1,5]))
database = MOBY_DICK_DATABASE

out = findFrequentItemsets(MOBY_DICK_DATABASE, 50)
for r in findRules(out, database,0.4):
    print(str(r[0])+"  ===>   "+str(r[1]))
    

        
